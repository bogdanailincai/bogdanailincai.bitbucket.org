/**
 * @author      Bogdan Ailincai <bogdan.ailincai@centric.eu>
 * @version     1.0
 * @since       2014-08-04
 * @see         http://pc-bailinca/RFEditor/rfeditor.pdf
 */
function RFEditor(editor) {
  /**
  * sets all the event listeners
  * this method must be called in order to use the editor
  * @public
  * @return void
  */
  try
  {
    var m = 0;
    var v = [];
    var x;
    for (var r = 0; r < 16; r+=3)
    {
      for (var g = 0; g < 16; g+=3)
      {    
        v[m] = [];
        for (var b = 0; b < 16; b+=3)
        {
          x = b/3;
          v[m][x] = (Math.pow(16, 0)*b + Math.pow(16, 1)*b)
                  + (Math.pow(16, 2)*g + Math.pow(16, 3)*g)
                  + (Math.pow(16, 4)*r + Math.pow(16, 5)*r);
          v[m][x] = v[m][x].toString(16);
          if (v[m][x] === 0)
          {
            v[m][x] = "000000";
          }
          else if (v[m][x].length == 4)
          {
            v[m][x] = "00" + v[m][x];
          }
          else if (v[m][x].length == 2)
          {
            v[m][x] = "0000" +  v[m][x];
          }
        }
        m++;
      }
    }

    for(var i = 0; i < 6; i++)
    {
      for(var j = 0; j < 18; j++)
      {
        editor.getElementsByTagName('table')[0].getElementsByTagName('tr')[i].innerHTML +=  '<td bgcolor=#' +  v[j][i] +'></td>';
      }
    }

    for(var i = 0; i < 6; i++)
    {
      for(var j = 18; j < 36; j++)
      {
        editor.getElementsByTagName('table')[0].getElementsByTagName('tr')[i+6].innerHTML +=  '<td bgcolor=#' +  v[j][i] +'></td>';
      }
    }

    var previewColor = function(event)
    {
      editor.getElementsByClassName('editorContainer_colorPickerColors')[0].getElementsByTagName('input')[1].value = event.currentTarget.getAttribute('bgcolor').substr(1,6).toUpperCase();
      editor.getElementsByClassName('editorContainer_colorPickerPreview')[0].style.backgroundColor = event.currentTarget.getAttribute('bgcolor');
    };

    var tds = editor.getElementsByClassName('editorContainer_colorPickerColors')[0].getElementsByTagName('table')[0].getElementsByTagName('td');
    for(var i = 0; i < tds.length; i++)
    {
      tds[i].addEventListener("mouseover", previewColor, false);
      tds[i].addEventListener("click", function(event){formatDoc('forecolor', event.currentTarget.getAttribute('bgcolor'));}, false);
    }

    var openComboList = function(event)
    {
      var combos = { editorContainer_fontname:"editorContainer_fontNames",
                      editorContainer_fontsize:"editorContainer_fontSizes",
                      editorContainer_colorPicker:"editorContainer_colorPickerColors"};
      var comboListClass = combos[event.currentTarget.className];
      saveSelection();
      hideOtherCombos(comboListClass);
      toggleDisplayForElement(comboListClass);
      editor.getElementsByClassName(comboListClass)[0].focus();
    };

    editor.getElementsByClassName("editorContainer_edit")[0].addEventListener("click", hideAllCombos, false);

    editor.getElementsByClassName("editorContainer_fontname")[0].addEventListener("click", openComboList, false);
    editor.getElementsByClassName("editorContainer_fontsize")[0].addEventListener("click", openComboList, false);
    editor.getElementsByClassName("editorContainer_colorPicker")[0].addEventListener("click", openComboList, false);

    var comboButtons = editor.getElementsByClassName('editorContainer_fontNames')[0].getElementsByTagName('button');
    for(var i = 0; i < comboButtons.length; i++)
    {
      comboButtons[i].addEventListener("click", function(event){formatDoc('fontname', event.currentTarget.textContent);}, false);
    }

    comboButtons = editor.getElementsByClassName('editorContainer_fontSizes')[0].getElementsByTagName('button');
    for(var i = 0; i < comboButtons.length; i++)
    {
      comboButtons[i].addEventListener("click", function(event){formatDoc('fontsize', event.currentTarget.textContent);}, false);
    }

    editor.getElementsByClassName("editorContainer_bold")[0].addEventListener("click", function(){formatDoc('bold');}, false);
    editor.getElementsByClassName("editorContainer_italic")[0].addEventListener("click", function(){formatDoc('italic');}, false);
    editor.getElementsByClassName("editorContainer_underline")[0].addEventListener("click", function(){formatDoc('underline');}, false);
    editor.getElementsByClassName("editorContainer_alignLeft")[0].addEventListener("click", function(){formatDoc('justifyleft');}, false);
    editor.getElementsByClassName("editorContainer_alignCenter")[0].addEventListener("click", function(){formatDoc('justifycenter');}, false);
    editor.getElementsByClassName("editorContainer_alignRight")[0].addEventListener("click", function(){formatDoc('justifyright');}, false);
    editor.getElementsByClassName("editorContainer_justify")[0].addEventListener("click", function(){formatDoc('justifyfull');}, false);
    editor.getElementsByClassName("editorContainer_bulletList")[0].addEventListener("click", function(){formatDoc('insertunorderedlist');}, false);
    editor.getElementsByClassName("editorContainer_http")[0].addEventListener("click", function(){formatDoc('createlink');}, false);
  } catch(errorInit)
  {
    var message = errorInit.message;
    
    if(message.indexOf("null") > -1)
    {
      alert("The editor's markup is incomplete. Please check the user manual at http://students.info.uaic.ro/~bogdan.ailincai/RFEditor/help.html and update the markup.");
    }
    else
    {
      alert(message);
    }
    return;
  }

  /**
  * stores the text selection when saveSelection is called
  * @private
  * @see https://developer.mozilla.org/en/docs/Web/API/Range
  * @see saveSelection
  */
  var selRange;

  /**
  * saves a Range object in selRange
  * @private
  * @return void
  * @see https://developer.mozilla.org/en/docs/Web/API/Range
  * @see selRange
  */
  function saveSelection() {
      var sel = window.getSelection();
      if (sel.getRangeAt && sel.rangeCount) 
      {
        selRange =  sel.getRangeAt(0); // for when the user selects multiple chunks of text
      }
      //allows the focus to be restored
  }

  /**
  * selects some text corresponding with the selRange variable
  * @private
  * @return void
  * @see https://developer.mozilla.org/en/docs/Web/API/Range
  * @see selRange
  */
  function restoreSelection() {
    if(selRange)
    {
      var sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(selRange);
    }
  }

  /**
  * convert a font size (1-7) value to a pixel value
  * @private
  * @param sizeValue
  * @return the font size (1-7) value converted to pixel value
  */
  function size2pixel(sizeValue)
  {
    console.log(sizeValue);
    switch (parseInt(sizeValue))
    {
      case 1:
        return 12;
      case 2:
        return 14;
      case 3:
        return 16;
      case 4:
        return 18;
      case 5:
        return 24;
      case 6:
        return 32;
      case 7:
        return 48;
    }
  }

  /**
  * convert a pixel value to a font size (1-7) value
  * @private
  * @param pixelValue
  * @return the pixel value translated to size (from 1 to 7)
  */
  function pixel2size(pixelValue)
  {
    if (pixelValue > 40)
    {
      return 7;
    }
    else if (pixelValue < 40 && pixelValue >= 28)
    {
      return 6;
    }
    else if (pixelValue < 28 && pixelValue >= 21)
    {
      return 5;
    }
    else if (pixelValue < 21 && pixelValue >= 17)
    {
      return 4;
    }
    else if (pixelValue < 17 && pixelValue >= 15)
    {
      return 3;
    }
    else if (pixelValue < 15 && pixelValue >= 13)
    {
      return 2;
    }
    return 1;
  }

  /**
  * execute operations on text
  * @private
  * @param sCmd command name (fontname/fontsize/bold/ etc.)
  * @param sValue value used in some commands (Arial/1/red/ etc.)
  * @return void
  * @see https://developer.mozilla.org/en-US/docs/Web/API/document.execCommand
  */
  function formatDoc(sCmd, sValue) {
    var xxClass = window.getSelection().anchorNode;
    while (xxClass && xxClass.className != 'editorContainerClass')
    {
      xxClass = xxClass.parentNode;
    }
    if (xxClass && xxClass != editor)
    {
      return;
    }
    if (sCmd == 'fontname' || sCmd == 'forecolor')
    {
      restoreSelection();
    }

    else if (sCmd == 'fontsize' && parseInt(sValue))
    {
      restoreSelection();
      toggleDisplayForElement('editorContainer_fontSizes');
      document.execCommand(sCmd, false, pixel2size(sValue));
      editor.getElementsByClassName("editorContainer_edit")[0].focus();
      return;
    }
    else if(sCmd == 'createlink')
    {
      hideAllCombos();
      var url = prompt('Write the URL here', 'http:\/\/');
      if (url && url!== '' && url!='http://')
      {
        document.execCommand(sCmd, false, url);
      }
      return;
    }
    saveSelection();
    restoreSelection();
    hideAllCombos();
    document.execCommand(sCmd, false, sValue);
    editor.getElementsByClassName("editorContainer_edit")[0].focus();
  }

  /**
  * get HTML code of edited text
  * @public
  * @return String (Example: "<p>Lorem ipsum</p>")
  */
  this.getMarkup = function()
  {
    var backup = editor.getElementsByClassName("editorContainer_edit")[0].innerHTML;
    var fontTags = (editor.getElementsByClassName("editorContainer_edit")[0]).getElementsByTagName('font');
    for (var i=0; i < fontTags.length; i++)
    {
      if(fontTags[i].getAttribute("size"))
      {
        fontTags[i].setAttribute("size", size2pixel(fontTags[i].getAttribute("size")));
      }
    }

    var pTags = (editor.getElementsByClassName("editorContainer_edit")[0]).getElementsByTagName('p');
    for (var i=0; i < pTags.length; i++)
    {
      if(pTags[i].getAttribute("style") !== null && pTags[i].getAttribute("style") !== undefined)
      {
        pTags[i].setAttribute("align", pTags[i].getAttribute("style").substring(12, pTags[i].getAttribute("style").length - 1));
        pTags[i].removeAttribute('style');
      }
    }

    var x = editor.getElementsByClassName("editorContainer_edit")[0].innerHTML;

    editor.getElementsByClassName("editorContainer_edit")[0].innerHTML = backup;
    return x;
  };

  /**
  * sets the content of the editor container
  * @public
  * @param htmlStr String containing the markup code to be introduced in the editor container
  * @return void
  */
  this.setMarkup = function(htmlStr)
  {    
    editor.getElementsByClassName("editorContainer_edit")[0].innerHTML = htmlStr;
    var fontTags = editor.getElementsByClassName("editorContainer_edit")[0].getElementsByTagName('font');
    for (var i=0; i < fontTags.length; i++)
    {
      if(fontTags[i].getAttribute("size"))
      {
        fontTags[i].setAttribute("size", pixel2size(fontTags[i].getAttribute("size")));
      }
    }

    var pTags = (editor.getElementsByClassName("editorContainer_edit")[0]).getElementsByTagName('p');
    for (var i=0; i < pTags.length; i++)
    {
      if(pTags[i].getAttribute("align"))
      {
        pTags[i].style.textAlign = pTags[i].getAttribute("align");
        pTags[i].removeAttribute('align');
      }
    }
  };

  /**
  * hides an element if it's visible, show it if it's not displayed
  * @private
  * @param id the id of the element to show or hide
  * @return void
  */
  function toggleDisplayForElement(givenClass) {
    var obj = editor.getElementsByClassName(givenClass)[0];

    if(obj.style.display != "block")
    {
      obj.style.display = 'block';
      if (window.innerHeight < (obj.offsetHeight + obj.offsetTop - document.body.scrollTop))
      {
        obj.style.top = String(obj.offsetTop - obj.offsetHeight - editor.getElementsByClassName('editorContainer_fontname')[0].offsetHeight) + "px";
      }
    }
    else
    {
      var initialYFontFamily = editor.getElementsByClassName('editorContainer_fontname')[0].offsetHeight
         + editor.getElementsByClassName('editorContainer_fontname')[0].offsetTop + "px";
      var initialYFontSize = editor.getElementsByClassName('editorContainer_fontsize')[0].offsetHeight
             + editor.getElementsByClassName('editorContainer_fontsize')[0].offsetTop + "px";
      var initialYFontColor = editor.getElementsByClassName('editorContainer_colorPicker')[0].offsetHeight
             + editor.getElementsByClassName('editorContainer_colorPicker')[0].offsetTop + "px";
      obj.style.top = (givenClass == 'editorContainer_fontNames'?
                        initialYFontFamily: (givenClass == 'editorContainer_fontSizes'?
                                            initialYFontSize: initialYFontColor));
      obj.style.display = "none";
    }
  }

  /**
  * hides all the combobox lists
  * @private
  * @return void
  */
  function hideAllCombos() {
    var initialYFontFamily = editor.getElementsByClassName('editorContainer_fontname')[0].offsetHeight
         + editor.getElementsByClassName('editorContainer_fontname')[0].offsetTop + "px";
    var initialYFontSize = editor.getElementsByClassName('editorContainer_fontsize')[0].offsetHeight
           + editor.getElementsByClassName('editorContainer_fontsize')[0].offsetTop + "px";
    var initialYFontColor = editor.getElementsByClassName('editorContainer_colorPicker')[0].offsetHeight
           + editor.getElementsByClassName('editorContainer_colorPicker')[0].offsetTop + "px";


    editor.getElementsByClassName("editorContainer_fontNames")[0].style.top = initialYFontFamily;
    editor.getElementsByClassName("editorContainer_fontSizes")[0].style.top = initialYFontSize;
    editor.getElementsByClassName("editorContainer_colorPickerColors")[0].style.top = initialYFontColor;

    editor.getElementsByClassName("editorContainer_fontNames")[0].style.display = "none";
    editor.getElementsByClassName("editorContainer_fontSizes")[0].style.display = "none";
    editor.getElementsByClassName("editorContainer_colorPickerColors")[0].style.display = "none";
  }

  /**
  * hides all the combobox lists except the one with the comboListId id
  * @private
  * @param comboListId the id of the combobox to keep from hiding
  * @return void
  */
  function hideOtherCombos(comboListClass){
    var initialYFontFamily = editor.getElementsByClassName('editorContainer_fontname')[0].offsetHeight
           + editor.getElementsByClassName('editorContainer_fontname')[0].offsetTop + "px";
    var initialYFontSize = editor.getElementsByClassName('editorContainer_fontsize')[0].offsetHeight
           + editor.getElementsByClassName('editorContainer_fontsize')[0].offsetTop + "px";
    var initialYFontColor = editor.getElementsByClassName('editorContainer_colorPicker')[0].offsetHeight
           + editor.getElementsByClassName('editorContainer_colorPicker')[0].offsetTop + "px";
    var classes = ["editorContainer_fontNames", "editorContainer_fontSizes", "editorContainer_colorPickerColors"];
    classes.splice(classes.indexOf(comboListClass), 1);
    for(var i = 0; i < classes.length; i++){

      editor.getElementsByClassName(classes[i])[0].style.top = (classes[i] == 'editorContainer_fontNames'?
                                                                initialYFontFamily: classes[i] == 'editorContainer_fontSizes'?
                                                                  initialYFontSize: initialYFontColor);
      editor.getElementsByClassName(classes[i])[0].style.display = "none";
    }
  }
}

var myRFEditors = {};

function initRFEditors()
{
  var myRFEditorsList = document.getElementsByClassName('editorContainerClass');
    for(var i = 0; i < myRFEditorsList.length; i++)
    {
      myRFEditors[myRFEditorsList[i].id] = new RFEditor(document.getElementById(myRFEditorsList[i].id));
    }
}